# aj's dotfiles

## Clone the repository
	git clone https://gitlab.com/ajsabandal/aj-dotfiles.git ~/dotfiles

## Run the install script
	sh ~/dotfiles/install.sh