# jump to root directory
cd ~

# install dependencies via apt 
sudo apt update
sudo apt install nodejs
sudo apt install npm

# install dependencies via npm
sudo npm install --global yarn
sudo npm install --global tldr